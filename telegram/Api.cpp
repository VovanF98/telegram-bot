#include <Poco/URI.h>
#include <iostream>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPSClientSession.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/SSLManager.h>
#include <Poco/Net/AcceptCertificateHandler.h>
#include "Api.h"
#include "JSONConverter.h"
#include "BotExceptions.h"


Api::Api(std::string token, std::string host): bot_token_(token), host_(host) {
    Poco::URI uri(host);
    if (uri.getScheme() == "https") {
        session_ = std::make_shared<Poco::Net::HTTPSClientSession>(uri.getHost(), uri.getPort());
    } else {
        session_ = std::make_shared<Poco::Net::HTTPClientSession>(uri.getHost(), uri.getPort());
    }

    bot_path_ = "bot" + token + "/";
}

std::vector<std::shared_ptr<ReceivedMessage>> Api::getUpdates(int offset, int timeout) {
    Poco::URI uri(host_ + bot_path_ + "getUpdates");
    if (offset)
        uri.addQueryParameter("offset", std::to_string(offset));
    if (timeout)
        uri.addQueryParameter("timeout", std::to_string(timeout));

    std::string path = uri.getPathAndQuery();

    Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET,
                                   path, Poco::Net::HTTPMessage::HTTP_1_1);

    session_->sendRequest(request);

    Poco::Net::HTTPResponse response;
    std::istream &is = session_->receiveResponse(response);

    Json::CharReaderBuilder reader;
    Json::Value json_response;
    Json::Reader reader1;
    bool c = reader1.parse(is, json_response);
    if (!c)
        throw ConnectionException("");

    return from_json(json_response);
}

void Api::sendMessage(Json::Value response) {
    Poco::URI uri(host_ + bot_path_ + "sendMessage");

    Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST,
                                   uri.getPathAndQuery(),
                                   Poco::Net::HTTPMessage::HTTP_1_1);

    Json::FastWriter writer;
    std::string response_str = writer.write(response);

    request.setContentType("application/json");
    request.setKeepAlive(true);
    request.setContentLength(response_str.size());

    session_->sendRequest(request) << response_str;
    std::cout << response_str << "\n";

    Poco::Net::HTTPResponse res;
    std::istream &is = session_->receiveResponse(res);
}

bool Api::GetMe() {
    Poco::URI uri(host_ + bot_path_ + "getMe");

    Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET,
                                   uri.getPathAndQuery(),
                                   Poco::Net::HTTPMessage::HTTP_1_1);

    session_->sendRequest(request);

    Poco::Net::HTTPResponse response;
    std::istream &is = session_->receiveResponse(response);;

    Json::CharReaderBuilder reader;
    Json::Value json_response;
    Json::Reader reader_response;
    bool ok = reader_response.parse(is, json_response);
    if (!ok)
        throw ConnectionException("");
    if (!json_response["ok"].asBool())
        throw ConnectionException(json_response["error_code"].asString());

    auto user = from_json_to_user(json_response);

    return json_response["ok"] == true;
}