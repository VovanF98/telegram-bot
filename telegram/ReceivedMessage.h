#ifndef TELEGRAM_INCOME_MESSAGE_H
#define TELEGRAM_INCOME_MESSAGE_H

#include <string>
#include <vector>
#include "Api.h"

class ReceivedMessage {
public:
    virtual ~ReceivedMessage() = default;
    int GetUpdateId();
    void SetUpdateId(int update_id);
    User GetAddresser();
    void SetAddresser(const User& user);
    int GetDate();
    void SetDate(int date);
    void SetChatId(int id);
    int GetChatId();
    int message_id = 0;
private:
    User addresser;
    int chat_id;
    int date;
    int update_id;
};

#endif
