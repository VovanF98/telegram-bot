#ifndef TELEGRAM_COMMANDS_H
#define TELEGRAM_COMMANDS_H

#include <memory>
#include "SentMessage.h"
#include "ReceivedMessage.h"
#include "SentTextMessage.h"
#include "ReceivedTextMessage.h"


std::shared_ptr<SentMessage> CommandRandom(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandEcho(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandWinter(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandStyleGuide(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandAbort(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandStop(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandGetMe(const std::shared_ptr<ReceivedTextMessage>& m);
std::shared_ptr<SentMessage> CommandInvalid(const std::shared_ptr<ReceivedTextMessage>& m);


#endif
