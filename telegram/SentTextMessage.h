#pragma once
#include "SentMessage.h"

class SentTextMessage: public SentMessage {
public:
    SentTextMessage();
    std::string GetText();
    void SetText(const std::string& text_);
private:
    std::string text;
};
