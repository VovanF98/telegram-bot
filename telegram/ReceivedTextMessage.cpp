#include "ReceivedTextMessage.h"

std::vector<BotCommand> ReceivedTextMessage::GetCommands() {
    return commands;
}

std::string ReceivedTextMessage::GetText() {
    return text;
}

void ReceivedTextMessage::SetText(const std::string& text_) {
    text = text_;
}

void ReceivedTextMessage::AddCommand(const BotCommand &bot_command) {
    commands.emplace_back(bot_command);
}

