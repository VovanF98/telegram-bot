#include "BotCommands.h"
#include "random"


std::shared_ptr<SentMessage> CommandRandom(const std::shared_ptr<ReceivedTextMessage>& message){
    auto message_to_send = SentTextMessage();
    message_to_send.SetChatId(message.get()->GetAddresser().id);
    std::random_device rand_device;
    std::mt19937 generate(rand_device());
    std::uniform_int_distribution<> distribute(-100000, 100000);
    std::string random_string_number = std::to_string(distribute(generate));
    message_to_send.SetText(random_string_number);
    return std::make_shared<SentTextMessage>(message_to_send);
}

std::shared_ptr<SentMessage> CommandEcho(const std::shared_ptr<ReceivedTextMessage>& message){
    auto message_to_send = SentTextMessage();
    message_to_send.SetChatId(message.get()->GetAddresser().id);
    std::string text_to_print = message.get()->GetText();
    message_to_send.SetText(text_to_print);
    return std::make_shared<SentTextMessage>(message_to_send);
}


std::shared_ptr<SentMessage> CommandWinter(const std::shared_ptr<ReceivedTextMessage>& message){
    auto message_to_send = SentTextMessage();
    message_to_send.SetChatId(message.get()->GetAddresser().id);
    std::string text_to_print = "Winter Is Coming...";
    message_to_send.SetText(text_to_print);
    return std::make_shared<SentTextMessage>(message_to_send);
}


std::shared_ptr<SentMessage> CommandStyleGuide(const std::shared_ptr<ReceivedTextMessage>& message){
    auto message_to_send = SentTextMessage();
    message_to_send.SetChatId(message.get()->GetAddresser().id);
    std::string text_to_print = "Joke";
    message_to_send.SetText(text_to_print);
    return std::make_shared<SentTextMessage>(message_to_send);
}

std::shared_ptr<SentMessage> CommandStop(const std::shared_ptr<ReceivedTextMessage>&){
    return nullptr;
}

std::shared_ptr<SentMessage> CommandAbort(const std::shared_ptr<ReceivedTextMessage>&){
    abort();
}

std::shared_ptr<SentMessage> CommandInvalid(const std::shared_ptr<ReceivedTextMessage>& message) {
    auto message_to_send = SentTextMessage();
    message_to_send.SetChatId(message.get()->GetAddresser().id);
    std::string text_to_print = "Hey! i'm not paid for doing that!";
    message_to_send.SetText(text_to_print);
    return std::make_shared<SentTextMessage>(message_to_send);
}