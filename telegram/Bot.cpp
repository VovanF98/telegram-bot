#include "Bot.h"
#include "BotCommands.h"
#include "JSONConverter.h"
#include "BotExceptions.h"
#include <fstream>


Bot::Bot(std::string token):api(token, "https://api.telegram.org/bot") {
    commands["/random"] = &CommandRandom;
    commands["/weather"] = &CommandWinter;
    commands["/styleguide"] = &CommandStyleGuide;
    commands["/abort"] = &CommandAbort;
    commands["/stop"] = &CommandStop;
}


std::shared_ptr<SentMessage> Bot::process_message(const std::shared_ptr<ReceivedMessage>& message){
    std::shared_ptr<ReceivedTextMessage> tm = std::dynamic_pointer_cast<ReceivedTextMessage>(message);
    if (!tm.get()->GetCommands().empty()) {
        auto bot_commands = tm.get()->GetCommands();
        if (commands.find(bot_commands[0].command) == commands.end()) {
            return CommandInvalid(tm);
        }
        return commands[bot_commands[0].command](tm);
    }
    return CommandEcho(tm);
}

void Bot::update_offset(int offset) {
    offset_ = offset;
    std::ofstream file("../offset.txt");
    file << offset;
    file.close();
}

void Bot::listen() {
    while (true) {
        auto messages = api.getUpdates(offset_, 0);
        for (const auto &m: messages) {
            update_offset(m.get()->GetUpdateId() + 1);
            try {
                auto processed_message = process_message(m);

                if (processed_message == nullptr) {
                    return;
                }

                auto message_to_send = to_json(processed_message);

                api.sendMessage(message_to_send);

            } catch (const BotStopException& ex) {
                return;
            }
        }
    }
}