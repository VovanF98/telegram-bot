#include <memory>
#include "JSONConverter.h"
#include "ReceivedTextMessage.h"


std::vector<std::shared_ptr<ReceivedMessage>> from_json(const Json::Value& obj){
    std::vector<std::shared_ptr<ReceivedMessage>> messages;
    for (auto json_entity : obj["result"]) {

        auto json_message = json_entity["message"];

        if (json_message.isMember("text")){
            ReceivedTextMessage received_message;
            std::string text_to_set = json_message["text"].asString();
            auto a = json_message["chat"]["id"].asInt();
            received_message.SetChatId(json_message["chat"]["id"].asInt());
            received_message.SetText(text_to_set);

            if (json_message.isMember("entities")){
                for (auto ent: json_message["entities"]) {
                    BotCommand bot_command;
                    bot_command.start_pos = ent["offset"].asInt();
                    bot_command.command = received_message.GetText().substr(size_t(ent["offset"].asInt()),
                                                                       size_t(ent["length"].asInt()));
                    received_message.AddCommand(bot_command);
                }
            }

            User user;
            user.first_name = json_message["from"]["first_name"].asString();
            user.last_name = json_message["from"]["last_name"].asString();
            user.is_bot = json_message["from"]["is_bot"].asBool();
            user.username = json_message["from"]["username"].asString();
            user.id = json_message["from"]["id"].asInt();

            received_message.SetAddresser(user);
            int date = json_message["date"].asInt();
            received_message.SetDate(date);
            int new_update_id = json_entity["update_id"].asInt();
            received_message.message_id = json_message["message_id"].asInt();
            received_message.SetUpdateId(new_update_id);
            messages.push_back(std::make_shared<ReceivedTextMessage>(received_message));

        }
    }
    return messages;
}

User from_json_to_user(const Json::Value& obj) {

    User user;
    user.first_name = obj["result"]["first_name"].asString();
    user.last_name = obj["result"]["last_name"].asString();
    user.is_bot = obj["result"]["is_bot"].asBool();
    user.username = obj["result"]["username"].asString();
    user.id = obj["result"]["id"].asInt();

    return user;
}

Json::Value to_json(std::shared_ptr<SentMessage>& sent_message) {
    Json::Value jv;
    if (sent_message->reply_id) {
        jv["reply_to_message_id"] = sent_message->reply_id;
    }
    if (sent_message.get()->GetActionName() == "sendMessage") {
        auto sent_text_message = std::dynamic_pointer_cast<SentTextMessage>(sent_message);
        jv["chat_id"] = sent_text_message.get()->GetChatId();
        jv["action"] = sent_text_message.get()->GetActionName();
        jv["text"] = sent_text_message.get()->GetText();

    }
    return jv;
}
