#ifndef TELEGRAM_RECIEVEDTEXTMESSAGE_H
#define TELEGRAM_RECIEVEDTEXTMESSAGE_H

#include "ReceivedMessage.h"

struct BotCommand{
    std::string command;
    int start_pos;
};


class ReceivedTextMessage: public ReceivedMessage {
public:
    std::string GetText();
    void SetText(const std::string& text);
    std::vector<BotCommand> GetCommands();
    void AddCommand(const BotCommand& bot_command);
private:
    std::string text;
    std::vector<BotCommand> commands;
};


#endif
