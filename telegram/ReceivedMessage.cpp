#include "ReceivedMessage.h"

User ReceivedMessage::GetAddresser() {
    return addresser;
}

int ReceivedMessage::GetUpdateId() {
    return update_id;
}

void ReceivedMessage::SetAddresser(const User &user) {
    addresser = user;
}

void ReceivedMessage::SetUpdateId(int update_id_) {
    update_id = update_id_;
}

int ReceivedMessage::GetDate() {
    return date;
}

void ReceivedMessage::SetDate(int date_) {
    date = date_;
}

void ReceivedMessage::SetChatId(int id) {
    chat_id = id;
}

int ReceivedMessage::GetChatId() {
    return chat_id;
}