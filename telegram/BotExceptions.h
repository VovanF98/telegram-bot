#ifndef TELEGRAM_BOTEXCEPTIONS_H
#define TELEGRAM_BOTEXCEPTIONS_H
#include <stdexcept>

class BotStopException: public std::logic_error {
public:
    explicit BotStopException(const std::string& message): logic_error(message) {}
};

class ConnectionException: public std::runtime_error {
public:
    explicit ConnectionException(const std::string& message): runtime_error(message) {}
};


#endif
