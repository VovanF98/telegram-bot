#include "SentTextMessage.h"
#include "../test/fake.h"

std::string SentTextMessage::GetText() {
    return text;
}

void SentTextMessage::SetText(const std::string &text_) {
    text = text_;
}

SentTextMessage::SentTextMessage() {
    std::string action_name = "sendMessage";
    SetActionName(action_name);
}