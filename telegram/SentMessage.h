#pragma once
#include <string>

class SentMessage {
public:
    virtual ~SentMessage() = default;
    std::string GetActionName();
    void SetActionName(const std::string& action_name);
    int GetChatId();
    void SetChatId(int id);
    int reply_id = 0;
private:
    std::string action;
    int chat_id;
};
