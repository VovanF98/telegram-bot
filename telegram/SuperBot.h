#ifndef TELEGRAM_BOT_BASE_H
#define TELEGRAM_BOT_BASE_H

#include <memory>
#include <iostream>
#include "SentMessage.h"
#include "ReceivedMessage.h"


class SuperBot {
public:
    virtual ~SuperBot() = default;
    virtual std::shared_ptr<SentMessage> process_message(const std::shared_ptr<ReceivedMessage>& m) = 0;
};

#endif
