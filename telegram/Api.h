#ifndef TELEGRAM_GETTER_H
#define TELEGRAM_GETTER_H

#include <memory>
#include <Poco/Net/HTTPSClientSession.h>
#include <json/json.h>

class ReceivedMessage;

struct User{
    std::string first_name;
    std::string last_name;
    std::string username;
    bool is_bot;
    int id;
};

class Api {
public:
    Api(std::string token, std::string uri_);
    std::vector<std::shared_ptr<ReceivedMessage>> getUpdates(int offset_ = 0, int timeout_ = 35);
    void sendMessage(Json::Value);
    bool GetMe();

private:
    std::shared_ptr<Poco::Net::HTTPClientSession> session_;
    std::string host_;
    std::string bot_path_;
    std::string bot_token_;
};


#endif
