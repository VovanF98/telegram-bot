#ifndef TELEGRAM_MY_BOT_H
#define TELEGRAM_MY_BOT_H

#include <map>
#include "SuperBot.h"
#include "Api.h"
#include "ReceivedTextMessage.h"

class Bot: public SuperBot {
public:
    typedef std::shared_ptr<SentMessage> (*command_type)(const std::shared_ptr<ReceivedTextMessage> &m);
    Bot(std::string token);
    std::shared_ptr<SentMessage> process_message(const std::shared_ptr<ReceivedMessage>& m) override;
    void listen();
    int offset_;
    void update_offset(int offset);
private:
    std::map<std::string, command_type> commands;
    Api api;

};


#endif
