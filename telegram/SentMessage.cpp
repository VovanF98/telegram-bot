#include "SentMessage.h"

std::string SentMessage::GetActionName() {
    return action;
}

int SentMessage::GetChatId() {
    return chat_id;
}

void SentMessage::SetActionName(const std::string& action_name_) {
    action = action_name_;
}

void SentMessage::SetChatId(int id) {
    chat_id = id;
}
