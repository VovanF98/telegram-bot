#pragma once

#include <json/json.h>
#include <iostream>
#include "ReceivedMessage.h"
#include "SentMessage.h"
#include "SentTextMessage.h"

std::vector<std::shared_ptr<ReceivedMessage>> from_json(const Json::Value& obj);
Json::Value to_json(std::shared_ptr<SentMessage>& received_message);
User from_json_to_user(const Json::Value& obj);

