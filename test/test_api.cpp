#include <catch.hpp>
#include "../telegram/Api.h"
#include "fake.h"
#include "../telegram/SentMessage.h"
#include "../telegram/ReceivedMessage.h"
#include "../telegram/JSONConverter.h"
#include "../telegram/BotExceptions.h"

TEST_CASE("Checking getMe") {
    telegram::FakeServer fake("Single getMe");
    fake.Start();
    std::string token = "123";
    auto api = std::make_shared<Api>(token, "http://localhost:8080/");
    auto botInfo = api.get()->GetMe();
    REQUIRE(botInfo == true);
    fake.StopAndCheckExpectations();
}

TEST_CASE("getMe error handling") {
    telegram::FakeServer fake("getMe error handling");
    fake.Start();
    std::string token = "123";
    auto api = std::make_shared<Api>(token, "http://localhost:8080/");
    REQUIRE_THROWS_AS(api.get()->GetMe(), ConnectionException);
    REQUIRE_THROWS_AS(api.get()->GetMe(), ConnectionException);
    fake.StopAndCheckExpectations();
}

TEST_CASE("Single getUpdates and send messages") {
    telegram::FakeServer fake("Single getUpdates and send messages");
    fake.Start();
    std::string token = "123";
    auto api = std::make_shared<Api>(token, "http://localhost:8080/");
    auto messages = api.get()->getUpdates(0, 0);

    auto sent_message = std::make_shared<SentTextMessage>();

    auto txt = "Hi!";
    sent_message.get()->SetText(txt);
    auto id = messages[0].get()->GetChatId();
    sent_message.get()->SetChatId(id);
    auto action_name = "sendMessage";
    sent_message.get()->SetActionName(action_name);
    auto sent_text_message = std::static_pointer_cast<SentMessage>(sent_message);
    auto object_to_send = to_json(sent_text_message);
    api.get()->sendMessage(object_to_send);

    auto text2 = "Reply";
    sent_message.get()->SetText(text2);
    sent_message.get()->SetChatId(messages[1].get()->GetChatId());
    auto action_name2 = "sendMessage";
    sent_message.get()->SetActionName(action_name2);
    sent_message.get()->reply_id = messages[1].get()->message_id;
    sent_text_message = std::static_pointer_cast<SentMessage>(sent_message);
    auto object_to_send2 = to_json(sent_text_message);
    api.get()->sendMessage(object_to_send2);

    auto text3 = "Reply";
    sent_message.get()->SetText(text3);
    sent_message.get()->SetChatId(messages[1].get()->GetChatId());
    auto action_name3 = "sendMessage";
    sent_message.get()->SetActionName(action_name3);
    sent_message.get()->reply_id = messages[1].get()->message_id;
    sent_text_message = std::static_pointer_cast<SentMessage>(sent_message);
    auto object_to_send3 = to_json(sent_text_message);
    api.get()->sendMessage(object_to_send3);

    fake.StopAndCheckExpectations();
}

TEST_CASE("Handle getUpdates offset") {
    telegram::FakeServer fake("Handle getUpdates offset");
    fake.Start();

    Api api = Api("123", "http://localhost:8080/");
    auto messages = api.getUpdates(0, 5);
    REQUIRE(messages.size() == 2);

    int offset = std::max(messages[1].get()->GetUpdateId(), messages[0].get()->GetUpdateId());
    ++offset;
    messages = api.getUpdates(offset, 5);
    REQUIRE(messages.size() == 0);

    messages = api.getUpdates(offset, 5);
    REQUIRE(messages.size() == 1);

    fake.StopAndCheckExpectations();
}